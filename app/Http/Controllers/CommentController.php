<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $comment = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Komen sudah masuk',
            'data'    => $comment  
        ], 200);

    }
    
     /**
     * show
     *
     *mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Comment',
            'data'    => $comment 
        ], 200);

    }
    
    /**
     * store
     *
     *   mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'     => $request->post_id
        ]);

        event(new CommentStoredEvent($comment));

        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Komen Berhasil',
                'data'    => $comment 
            ]);

            //Dikirim ke pemilik post
           // Mail::to($comment->post->user->email)->send(new TestingEmail($comment));
            //Dikirim ke yang comment
          //  Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     *   mixed $request
     *  mixed $post
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comment::find($id);

        if($comment) {

            if($comment->$user_id != $user_id)
            {
                 return response()->json([
                'success' => false,
                'message' => 'Ini bukan data post Anda!'
            ], 403);     
    }
            $comment->update([
                'content'     => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Content Updated',
                'data'    => $comment 
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     *   mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        if($comment) {

                if($comment->$user_id != $user_id)
                {
                     return response()->json([
                    'success' => false,
                    'message' => 'Ini bukan data post Anda!'
                ], 403);     
        }

            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}