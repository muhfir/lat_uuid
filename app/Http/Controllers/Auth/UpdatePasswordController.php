<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'email' => 'required',
        'email' => 'required|confirmed|min:6'
    ]);
    if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
    }
    $user = User::where('email', $reques->email)->first();

    $user->update([
        'password' => Hash::make($request->password)
    ]);

    return response()->jason([
        'succes' => true,
        'message' => 'Password sudah diubah',
        'data' => $user,
         ]);
    }
}
