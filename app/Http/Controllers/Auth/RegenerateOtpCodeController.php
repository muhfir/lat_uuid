<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\OtpCode;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'   => 'required',
    ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();
        if(!$user->otpCode){
            $user->otpCode->delete();
        }
       

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp' , $otp)->first();
        } while($check);

        $validUntil = Carbon::now()->addMinutes(10);
        $otp_code = OtpCode::create([
            'otp' => $otp, 
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        return response()->jason([
            'succes' => true,
            'message' => 'Otp sudah diregenerate, silakan cek email Anda',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code,
            ]
            ]);
    }
}
