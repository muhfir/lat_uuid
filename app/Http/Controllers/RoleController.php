<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $role = Role::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data sudah masuk',
            'data'    => $role  
        ], 200);

    }
    
     /**
     * show
     *
     *mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $role = Post::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $role
        ], 200);

    }
    
    /**
     * store
     *
     *   mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name'     => $request->name,
         ]);

        //success save to database
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'Nama Berhasil',
                'data'    => $role  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Name Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     *   mixed $request
     *  mixed $post
     * @return void
     */
    public function update(Request $request, Role $role)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $role = Role::findOrFail($role->id);

        if($role) {

            //update post
            $role->update([
                'name'     => $request->name,
           ]);

            return response()->json([
                'success' => true,
                'message' => 'Name Updated',
                'data'    => $role
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Name Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     *   mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $role = Role::findOrfail($id);

        if($role) {

            //delete post
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Name Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Name Not Found',
        ], 404);
    }
}