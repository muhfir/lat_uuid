<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use Illuminate\Support\Str;

class OtpCode extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id' ;
    protected $keyType = ['string'];
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
        }
        $model->role_id= Role::where('name' , 'Author')->first()->id;
    });

}
public function user(){
    return $this->belongsTo('App\User');
}
}
