<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{    
   public function __construct()
   {
       return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
   }
   
   
       /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $post = Post::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Data sudah masuk',
            'data'    => $post  
        ], 200);

    }
    
     /**
     * show
     *
     *mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $post = Post::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $post 
        ], 200);

    }
    
    /**
     * store
     *
     *   mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

       // $user = auth()->user();

        $post = Post::create([
            'title'     => $request->title,
            'description'   => $request->description,
            //'user_id' => $user->id
        ]);

        //success save to database
        if($post) {

            return response()->json([
                'success' => true,
                'message' => 'Data Post Berhasil',
                'data'    => $post  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     *   mixed $request
     *  mixed $post
     * @return void
     */
    public function update(Request $request, Post $post)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = Post::find($id);

        

        if($post) {

            $user = auth()->user();
            if($post->$user_id != $user_id)
            {
                 return response()->json([
                'success' => false,
                'message' => 'Ini bukan data post Anda!'
            ], 403);     
    }

        $post->update([
           'title'     => $request->title,
           'description'   => $request->description
        ]);         

    }
        return response()->json([
         'success' => true,
         'message' => 'Data dengan judul : ' . $post->title .  'berhasil ditemukan' ,
         'data' => $post
        ]);

           
        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' . $id .  'tidak ditemukan' ,
        ], 404);      


    }
    
    /**
     * destroy
     *
     *   mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $post = Post::findOrfail($id);

        if($post) {

            if($post->$user_id != $user_id)
            {
                 return response()->json([
                'success' => false,
                'message' => 'Ini bukan data post Anda!'
            ], 403);     
    }
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}