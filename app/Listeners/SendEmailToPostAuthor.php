<?php

namespace App\Listeners;

use App\Mail\TestingEmail;
use App\Events\CommentStoredEvent;
use Illmuninate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToPostAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentStoredEvent  $event
     * @return void
     */
    public function handle(CommentStoredEvent $event)
    {
        Mail::to($event->comment->post->user->email)->send(new TestingEmail($event->comment));
    }
}
